#------------------- Definition of class Binary Tree Node  --------------------------

class BinaryTreeNode:
	def __init__(self, priority = None, info = None):
		self.priority = priority
		self.info = info
		self.parent = None
		self.leftChild = None
		self.rightChild = None

#---------------------- End of class BinaryTreeNode --------------------------------


#-------------------- Definition of class Binary Tree ------------------------------

class BinaryTree:
	def __init__(self):
                self.root = None

                
	# Adds a new node to tree
	def addNode(self, priority, info):
		node = BinaryTreeNode(priority, info)
		if self.root is None:
			self.root = node
		else:
			BinaryTree.addNodeHelper(self.root, node)


	# Helper function to add new node (recursive) 
	def addNodeHelper(rootNode, node):
		if node.priority < rootNode.priority:
			#If root node has no left child, add this node as left child
			if rootNode.leftChild is None:
                                node.parent = rootNode
                                rootNode.leftChild = node
			else:
				BinaryTree.addNodeHelper(rootNode.leftChild, node)
		else:
			#If root node has no right child, add this node as right child
			if rootNode.rightChild is None:
                                node.parent = rootNode
                                rootNode.rightChild = node
			else:
				BinaryTree.addNodeHelper(rootNode.rightChild, node)



	# Tree inorder traversal
	def traversal(self):
		BinaryTree.traversalHelper(self.root)

	
	# Helper function for tree traversal(inorder)
	def traversalHelper(rootNode):
		if rootNode is None:
			return
		else:
			BinaryTree.traversalHelper(rootNode.leftChild)
			print(rootNode.priority)
			BinaryTree.traversalHelper(rootNode.rightChild)



	# Find node by priority and returns it
	def findNode(self, priority):
		currentNode = self.root
		while currentNode.priority != priority:
			# If desired priority is less than current node's priority, go down the left subtree
			if priority < currentNode.priority:
				if currentNode.leftChild is None:
					# Desired node not found in left subtree, returns object None
					return None
				else:
					currentNode = currentNode.leftChild
			# If desired priority is greather than current node's priority, go down the right subtree
			else:
				if currentNode.rightChild is None:
					# Desired node not found in right subtree, returns object None
					return None
				else:
					currentNode = currentNode.rightChild
		return currentNode



	# Function to remove a node
	def removeNode(self, node):
		if not (node is None):
			if BinaryTree.countChilds(node) == 0:
				BinaryTree.rmNodeWithoutChildren(self, node)
				return
			if BinaryTree.countChilds(node) == 1:
				BinaryTree.rmNodeWithOneChild(self, node)
				return
			if BinaryTree.countChilds(node) == 2:
				BinaryTree.rmNodeWithTwoChildren(self, node)
		else:
			print("Node not found")
		


	# Helper function to remove a node which not have children		
	def rmNodeWithoutChildren(tree, node):
		if node is tree.root:
			tree.root = None
			return
		else:
			if node is node.parent.leftChild:
				node.parent.leftChild = None
			else:
				node.parent.rightChild = None
				del node
				return


	# Helper function to remove a node with one child
	def rmNodeWithOneChild(tree, node):
		if node is tree.root:
			if tree.root.rightChild is None:
				tree.root = tree.root.leftChild
			else:
				tree.root = tree.root.rightChild
			tree.root.parent = None
			return
		else:
			parentNode = node.parent 
			childNode = BinaryTree.getChild(node)
			if node is parentNode.leftChild:
				parentNode.leftChild = childNode
				childNode.parent = parentNode
				del node
			else:
				parentNode.rightChild = childNode
				childNode.parent = parentNode
				del node
			return


	# Helper function to remove a node with two children
	def rmNodeWithTwoChildren(tree, node):
		newNode = BinaryTree.findLeftmostChild(node.rightChild)
		node.priority = newNode.priority
		node.info = newNode.info
		if BinaryTree.countChilds(newNode) == 0:
			BinaryTree.rmNodeWithoutChildren(tree, newNode)
		else:
			BinaryTree.rmNodeWithOneChild(tree, newNode)

	
	
	# Helper function for remove
	# Looking the most left child of rootNode and returns it
	def findLeftmostChild(rootNode):
		currentNode = rootNode
		while currentNode.leftChild:
			currentNode = currentNode.leftChild

		return currentNode


	# Function to remove a node by priority
	def removeNodeByPriority(self, priority):
		node = self.findNode(priority)
		if node is None:
			print("Node with priority {0} not found".format(priority))
			return
		else:
			self.removeNode(node)



	# Delete subtree from binary tree
	def deleteSubtree(self, subtreeRoot):
		BinaryTree.rmNodeWithoutChildren(self, subtreeRoot)



	# Helper function
	# Get node's child node, when node have one child
	def getChild(node):
		if node.leftChild is None:
			return node.rightChild
		else:
			return node.leftChild


	
	# Number of node's children
	def countChilds(node):
		counter = 2
		if node.leftChild is None:
			counter -= 1
		if node.rightChild is None:
			counter -= 1
		return counter		

 

#------------------------------ End of class BinaryTree  ------------------------------
